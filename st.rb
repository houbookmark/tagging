# key=["cafe.naver","cafe.daum","blog.naver","blog.daum","stackoverflow"]
# url = "http://m.blog.naver.com/jsjung1999/221152964968"
url = "https://m.blog.naver.com/jsjung1999/221152964968"
# url = "https://blog.naver.com/jsjung1999/221152964968"
# url = "http://m.blog.daum.net/pdi134/16122375"
# url = "http://m.blog.daum.net/shinbarksa/2369"
# url = "http://cafe.naver.com/appleiphone/4234984"
# url = "https://m.cafe.naver.com/PopularArticleRead.nhn?clubid=26872383&articleid=5926"
url = "danceple.com"

def check_url(url)
  crawl_hash={
  #   Url name      => ["body name","tag name", "Mobile Mode"]
    "cafe.naver"    => ["#ct",nil,true],
    "cafe.daum"     => ["#daumWrap",nil,true],
    "blog.naver"    => [".se_component_wrap","list_tag",true],
    "blog.daum"     => ["#daumWrap",nil,true],
    "stackoverflow" => ["body",nil,false]
  }
  # # Initialize
  # crawl_tag = "body"
  # mobile_mode = false

  crawl_hash.each do |key, val|
    unless url[key].nil?
      # crawl_tag = val[0]
      # mobile_mode = val[1]
      # break;
      # p crawl_tag, mobile_mode
      return [key, val]
    end
  end
  
  return ["body", nil, false]
end
# To do : When Mobile Mode add to "m." between "http://" and "url"
